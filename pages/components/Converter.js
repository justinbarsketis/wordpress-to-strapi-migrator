import Head from 'next/head'
import { useState } from 'react'

import Buttons from './Buttons.js'
import { getWordpressPosts, convertMarkdown } from '../hooks/Wordpress.js'
import {
  addPost,
  getPost,
  getOldPost,
  addOldPost,
  getAllPost
} from '../hooks/Strapi.js'

import { Button, Text } from '@chakra-ui/react'

const Converter = () => {
  const [loading, setLoading] = useState(false)
  const [outputLog, setOutputLog] = useState('')
  const [postList, setPostList] = useState([])

  const downloadPost = async () => {
    setOutputLog('Downloading posts...')
    setLoading(true)

    const postResponse = await getWordpressPosts()
    setOutputLog('Downloading posts...done')
    setPostList(postResponse.postList)

    //remove duplicates from postList

    // const uniquePostList = postList.filter()

    setOutputLog('Converting posts shortcodes to markdown...')
    const markDownResponse = await convertMarkdown({
      postList: postResponse.postList
    })

    setLoading(false)
    setOutputLog(`Markdown Scrape Successful`)
    setPostList(markDownResponse.postList)

    // await writePostJson(postResponse.postList)
  }

  const uploadPost = async () => {
    setLoading(true)
    setOutputLog('Uploading posts...')
    for (let postCount in postList) {
      if (postCount > 1) {
        // set to 0 to test 1 post
        console.log('max reached')
        break
      }
      if (postList[postCount].markdown) {
        // check if post already exist in strapi
        const response = await getPost(postList[postCount].slug)

        if (response.data.length === 0) {
          //if post does not exist in strapi, add post to strapi
          const response = await addPost(postList[postCount])
          console.log(response)
          console.log(postCount + ' post added')
        } else {
          console.log('post already exist')
        }
      }
    }
    setLoading(false)
    setOutputLog('Uploading posts...done')
  }

  const getAllOldPost = async () => {
    setLoading(true)
    setOutputLog('Scraping old posts...')
    const response = await getAllPost()
    console.log(response)
    setOutputLog('Scraping old posts...done')
    setPostList(response)
    setLoading(false)
  }

  const uploadStrapi = async () => {
    console.log(postList)
    setLoading(true)
    console.log(postList.length)
    setOutputLog('Uploading old posts...')
    const duplicates = []
    for (let postCount in postList) {
      console.log(postCount)
      console.log(postList[postCount])
      // if (postCount > 1) {
      //   // set to 0 to test 1 post
      //   console.log(postList[postCount])
      //   console.log('max reached')
      //   break
      // }

      // check if post already exist in strapi
      const response = await getOldPost(postList[postCount].slug)
      console.log(response)

      if (response.data.length === 0) {
        // if post does not exist in strapi, add post to strapi
        const response = await addOldPost(postList[postCount])
        console.log(response)

        console.log(postCount + ' post added')
      } else {
        duplicates.push(postList[postCount].slug)
        console.log('post already exist')
      }
    }
    setLoading(false)
    setOutputLog('Uploading old posts...done')
    console.log(duplicates)
  }

  return (
    <div>
      <Head>
        <title>Wordpress to Strapi Migrator</title>
      </Head>
      <header>
        <h1>Wordpres -> Strapi</h1>
      </header>
      <main>
        <p>
          This is a tool to migrate Wordpress to Strapi. Its custom to my
          purpose, but perhaps you can find some nuggets here. Other tools I did
          not like as much, as they didn't render shortcodes from plugins like
          VS Composer in Wordpress. This tool uses Pupeteer to crawl the
          Wordpress site and convert the already rendered codes to markdown.
        </p>
        <Buttons
          loading={loading}
          downloadPost={downloadPost}
          uploadPost={uploadPost}
          getAllOldPost={getAllOldPost}
          uploadStrapi={uploadStrapi}
        />
        <Text>{outputLog}</Text>
      </main>
    </div>
  )
}

export default Converter
