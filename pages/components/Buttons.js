import { Button, Text } from '@chakra-ui/react'

const Buttons = ({
  downloadPost,
  loading,
  uploadPost,
  getAllOldPost,
  uploadStrapi
}) => {
  return (
    <>
      <Text>
        Default Wordpress Route: {process.env.NEXT_PUBLIC_WORDPRESS_API_URL}
        /wp-json/v2/posts
      </Text>
      <Text>
        Default Strapi Route: {process.env.NEXT_PUBLIC_STRAPI_API_URL}
        /api/articles
      </Text>
      {!loading && (
        <Button m={5} onClick={downloadPost} colorScheme="teal">
          Download Post
        </Button>
      )}
      {loading && (
        <Button m={5} isLoading colorScheme="teal">
          Transfer Posts
        </Button>
      )}
      {!loading && (
        <Button m={5} onClick={uploadPost} colorScheme="teal">
          Upload Post
        </Button>
      )}
      {loading && (
        <Button isLoading colorScheme="teal">
          Upload Post
        </Button>
      )}
      {!loading && (
        <Button m={5} onClick={getAllOldPost} colorScheme="teal">
          Scrape Old Strapi Post
        </Button>
      )}
      {loading && (
        <Button isLoading colorScheme="teal">
          Scrape Old Strapi Post
        </Button>
      )}
      {!loading && (
        <Button m={5} onClick={uploadStrapi} colorScheme="teal">
          Upload Old Strapi Post
        </Button>
      )}
      {loading && (
        <Button isLoading colorScheme="teal">
          Upload Old Strapi Post
        </Button>
      )}
    </>
  )
}
export default Buttons
