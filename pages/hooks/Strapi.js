// const mime = require('mime-types')

export const getAllPost = async () => {
  const strapiPath = `${process.env.NEXT_PUBLIC_OLD_STRAPI_API_URL}/articles`

  const response = await fetch(strapiPath, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  })
  return await response.json()
}
export const getPost = async (slug) => {
  const strapiPath = `${process.env.NEXT_PUBLIC_STRAPI_API_URL}/api/articles?filters[slug][$eq]=${slug}`

  const response = await fetch(strapiPath, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  })
  return await response.json()
}
export const getOldPost = async (slug) => {
  const strapiPath = `${process.env.NEXT_PUBLIC_STRAPI_API_URL}/api/articles?filters[slug][$eq]=${slug}`

  const response = await fetch(strapiPath, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  })
  return await response.json()
}
export const addPost = async (post) => {
  const strapiPath = `${process.env.NEXT_PUBLIC_STRAPI_API_URL}/api/articles`
  const keywords = []
  // Change Keywords from Yoast from post Id's to values
  for (let i in post.yst_prominent_words) {
    if (post.yst_prominent_words[i]) {
      const wordpressPath = `${process.env.NEXT_PUBLIC_WORDPRESS_API_URL}/wp-json/wp/v2/yst_prominent_words/${post.yst_prominent_words[i]}`

      const response = await fetch(wordpressPath, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        }
      })
      const json = await response.json()
      if (response.status === 404) {
        break
      }
      // post.yst_prominent_words[i] = { keyword: json.name }
      keywords.push({ keyword: json.name })
    }
  }

  // Our description had extra tp ags not needed
  const removeHtmlTags = (str) => {
    return str.replace(/<[^>]*>/g, '')
  }
  // download image and meta data from wordpress
  // I had to download images locally to the server for some reason, passing blobs, streams and doing it client side had issues with CORS or the Strapi media upload api
  const downloadImage = async () => {
    const wordpressPath = `${process.env.NEXT_PUBLIC_WORDPRESS_API_URL}/wp-json/wp/v2/media/${post.featured_media}`

    const response = await fetch(wordpressPath, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    })

    const imageJson = await response.json()
    console.log(imageJson)
    console.log(imageJson.source_url)
    const fileExt = imageJson.media_details.file.split('.')
    const fileName = `${post.slug}.${fileExt[fileExt.length - 1]}`

    console.log('Filname is ' + fileName)
    // download file locally
    await fetch('api/download_image', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        url: imageJson.source_url,
        fileName
      })
    })

    //retrieve file
    const imageBlob = await fetch(`./images/${fileName}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'image/jpeg'
      }
    }).then((response) => response.blob())

    return { imageBlob, fileName, imageJson }
  }

  // JSON Format for our Strapi Installtion
  const newPost = {
    data: {
      title: post.title.rendered,
      description: removeHtmlTags(post.excerpt.rendered),
      content: post.markdown.markdown,
      slug: post.slug,
      //   categories: req.body.categories,
      author: 2,
      keywords: keywords,
      isSupportArticle: false,
      status: 'draft',
      createdAt: post.date,
      updatedAt: post.modified,
      publishedAt: post.date
    }
  }
  const image = await downloadImage()

  // Strapi requires Form Data for image upload
  const formData = new FormData()
  formData.append('files.image', image.imageBlob, image.fileName)
  formData.append('data', JSON.stringify(newPost.data))
  console.log('vaildation success')

  const response = await fetch(strapiPath, {
    method: 'POST',
    body: formData
  })
  console.log(response)

  const json = await response.json()
  if (response.error) {
    console.log(response.error)
    return json
  } else {
    return json
  }
}

export const addOldPost = async (post) => {
  const strapiPath = `${process.env.NEXT_PUBLIC_STRAPI_API_URL}/api/articles`
  // const keywords = []
  // // Change Keywords from Yoast from post Id's to values

  // // download image and meta data from wordpress
  // // I had to download images locally to the server for some reason, passing blobs, streams and doing it client side had issues with CORS or the Strapi media upload api
  const downloadImage = async (post) => {
    const strapiPath = `https://expatinsurance.imgix.net/img/articles/${post.imageUrl}`
    console.log(strapiPath)
    // image path
    //imageUrl

    // const response = await fetch(strapiPath, {
    //   method: 'GET',
    //   headers: {
    //     'Content-Type': 'application/json'
    //   }
    // })

    // const imageJson = await response.json()
    // console.log(imageJson)
    // console.log(imageJson.source_url)
    if (post.imageUrl === null) {
      return
    }
    const fileExt = post.imageUrl.split('.')
    const fileName = `${post.slug}.${fileExt[fileExt.length - 1]}`

    console.log('Filname is ' + fileName)
    // download file locally
    await fetch('api/download_image', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        url: strapiPath,
        fileName
      })
    })

    //retrieve file
    const imageBlob = await fetch(`./images/${fileName}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'image/jpeg'
      }
    }).then((response) => response.blob())

    return { imageBlob, fileName }
  }

  // // JSON Format for our Strapi Installtion
  const newPost = {
    data: {
      title: post.title,
      description: post.description,
      content: post.content,
      slug: post.slug,
      //   categories: req.body.categories,
      author: 2,
      keywords: [{ keyword: 'expat insurance' }],
      isSupportArticle: false,
      createdAt: post.createdAt,
      updatedAt: post.updatedAt,
      publishedAt: post.publishedAt
    }
  }
  const image = await downloadImage(post)
  if (image === undefined) {
    return
  }

  // // Strapi requires Form Data for image upload
  const formData = new FormData()
  formData.append('files.image', image.imageBlob, image.fileName)
  formData.append('data', JSON.stringify(newPost.data))
  // console.log('vaildation success')

  const response = await fetch(strapiPath, {
    method: 'POST',
    body: formData
  })
  console.log(response)

  const json = await response.json()
  if (response.error) {
    console.log(response.error)
    return json
  } else {
    return json
  }
}

// formData.append(
//   'files.fileInfo',
//   '{"caption":"caption text","alternativeText":"alternative text"}'
// )
// formData.append(
//   'image.fileInfo',
//   '{"caption":"caption text","alternativeText":"alternative text"}'
// )
// formData.append('files.image.caption', 'caption text')
// formData.append(
//   'fileInfo',
//   `{"caption":${image.imageJson.title.rendered},"alternativeText":${image.imageJson.caption.alt_text}}`
// )
// formData.append(
//   'fileInfo',
//   '{"caption":"caption text","alternativeText":"alternative text"}'
// )
// formData.append(
//   'files.image.fileInfo',
//   `{"caption":"test","alternativeText":"test"}`
// )
// formData.append(
//   'data.fileInfo',
//   '{"caption":"caption text","alternativeText":"alternative text"}'
// )
