export const getWordpressPosts = async () => {
  // call wordpress_id_list endpoint to get list of post ids
  const response = await fetch(`/api/wordpress_id_list`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  }).then((response) => response.json())

  return response
}

export const getWordpressMarkDown = (slug) => {
  return fetch(`/api/wordpress_download_post`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ slug })
  }).then((response) => response.json())
}

export const convertMarkdown = async ({ postList }) => {
  const errorPost = []
  for (let postCount in postList) {
    const postMarkdown = await fetch(`/api/wordpress_download_post`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ post: postList[postCount] })
    }).then((response) => response.json())
    if (postMarkdown.error) {
      errorPost.push({
        slug: postList[postCount].slug,
        success: false,
        message: postMarkdown.error
      })
    } else {
      postList[postCount].markdown = postMarkdown
    }
  }

  return { postList, errorPost }
}
