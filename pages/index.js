import Head from 'next/head'
import {
  List,
  ListItem,
  ListIcon,
  OrderedList,
  UnorderedList,
  Button,
  Input,
  InputGroup,
  InputRightAddon,
  InputLeftAddon,
  Spinner,
  ReactMarkdown
} from '@chakra-ui/react'
import { useState } from 'react'
import Converter from './components/Converter'

const Home = () => {
  return <Converter />
}

export default Home
