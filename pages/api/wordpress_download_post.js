// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

// take in url of website
// avoid duplicates
// catch errors
// create json entry
// create folder
// download images to folder
// crawl with pupeteer for markdown

// instead of local download, upload images directly to strapi, copy markdown directly, no middle man
import puppeteer from 'puppeteer'
import TurndownService from 'turndown'
// https://justmarkup.com/articles/2019-01-04-using-puppeteer-to-crawl-pages-and-save-them-as-markdown-files/
export default async function handler(req, res) {
  if (req.method === 'POST') {
    try {
      const url = `${process.env.NEXT_PUBLIC_WORDPRESS_API_URL}/${req.body.post.slug}`
      const browser = await puppeteer.launch()
      const page = await browser.newPage()
      const turndown = new TurndownService()

      console.log('Mardown Scraper', url)
      const response = await page.goto(url, {
        waitUntil: 'networkidle2'
      })

      if (page.url().includes('expatinsurance.com')) {
        console.log('Redirected')
        browser.close()
        res.status(500).json({ error: 'Redirected Page' })
        return
      }

      await page.waitForSelector('.article--content')

      const contentSelector = '.article--content'
      await page.waitForSelector(contentSelector, { timeout: 1500 })
      const pageContent = await page.$eval(
        contentSelector,
        (contentSelector) => contentSelector.innerHTML
      )
      let pageContentMarkdown = turndown.turndown(pageContent)

      browser.close()

      res.status(200).json({ markdown: pageContentMarkdown })
    } catch (error) {
      console.log(error)
      res.status(500).json({ error: 'sumthing wong' })
    }
  }
}
