import fs from 'fs'

export default async function handler(req, res) {
  if (req.method === 'POST') {
    const { fileName } = req.body
    const filePath = `./public/images/${fileName}`

    const saveFile = async (image) => {
      if (await fs.existsSync(filePath)) {
        // await fs.unlinkSync('./public/image.jpeg')
        return res.status(200).send('Image Already Downloaded')
      }

      await fs.writeFileSync(filePath, image)

      return
    }
    console.log('Downloading Image' + req.body.url)
    const image = await fetch(req.body.url)
    const imageBuffer = await image.buffer()

    try {
      await saveFile(imageBuffer)
    } catch (err) {
      console.log(err)
    }

    return res.status(200).send('Image Downloaded')
  } else {
    return res.status(400).send('Bad Request')
  }
}
