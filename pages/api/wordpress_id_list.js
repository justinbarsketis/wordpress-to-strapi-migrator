// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

// take in url of website
// iterate through all post id's, using recursion?
// avoid duplicates
// catch errors
// return json list

export default async function handler(req, res) {
  if (req.method === 'GET') {
    // Process a POST request
    const url = `${process.env.NEXT_PUBLIC_WORDPRESS_API_URL}/wp-json/wp/v2/posts`

    try {
      const response = await fetch(url, {
        method: 'GET',
        headers: {
          Accept: 'application/json'
        }
      })

      const totalPost = await response.headers.get('x-wp-total')
      const totalPages = await response.headers.get('x-wp-totalpages')

      const iteratePost = async (
        totalPost,
        totalPages,
        currentPage,
        postList
      ) => {
        if (totalPages < currentPage) {
          return postList
        } else {
          try {
            const response = await fetch(`${url}?page=${currentPage}`, {
              method: 'GET',
              headers: {
                Accept: 'application/json'
              }
            })

            // Return 200 if everything is successful
            const pagePost = await response.json()
            pagePost.map((item) => {
              postList.push(item)
            })
          } catch (err) {
            console.log(err)
          }
          return iteratePost(totalPost, totalPages, currentPage + 1, postList)
        }
      }

      const data = await iteratePost(totalPost, totalPages, 1, [])
      const postResponse = { totalPost, totalPages, postList: data }

      return res.status(200).send(postResponse)
    } catch (err) {
      res.status(500).json({ error: 'failed to load data' })
    }
  } else {
    res.status(500).json({ error: 'Invalid Request' })
  }
}
