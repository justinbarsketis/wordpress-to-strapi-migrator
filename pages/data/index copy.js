import Head from 'next/head'
import {
  List,
  ListItem,
  ListIcon,
  OrderedList,
  UnorderedList,
  Button,
  Input,
  InputGroup,
  InputRightAddon,
  InputLeftAddon,
  Spinner,
  ReactMarkdown
} from '@chakra-ui/react'
import { useState } from 'react'

import ChakraUIRenderer from 'chakra-ui-markdown-renderer'

export default function Home() {
  const [postList, setPostList] = useState([])
  const [postInfo, setPostInfo] = useState({ totalPages: 0, totalPost: 0 })
  const [progress, setProgress] = useState(0)
  const [loading, setLoading] = useState(false)
  const [step, setStep] = useState('crawl')
  const [url, setUrl] = useState('example.com')

  const getPostJson = async () => {
    try {
      setLoading(true)
      const response = await fetch(`/api/wordpress_id_list`)
      const json = await response.json()
      setPostList(json.postList)
      setPostInfo({ totalPages: json.totalPages, totalPost: json.totalPost })
      setLoading(false)
      setStep('backup')
    } catch (error) {
      setLoading('null')
    }
  }

  const writePostJson = async (postList) => {
    //write file to disk of postList  and postInfo
    try {
      FileSystem.writeFileSync(
        `${process.env.NEXT_PUBLIC_STRAPI_API_URL}/api/articles.json`,
        JSON.stringify(postList)
      )
      FileSystem.writeFileSync(
        `${process.env.NEXT_PUBLIC_STRAPI_API_URL}/api/articles_info.json`,
        JSON.stringify(postInfo)
      )
      setStep('done')
    } catch (error) {
      setLoading('null')
    }
  }

  const convertMarkdown = async () => {
    for (let postCount in postList) {
      setProgress(postCount)
      const postMarkdown = await scrapeMarkdown(postList[postCount])
      const newPostList = [...postList]
      newPostList[postCount].markdown = postMarkdown
      setPostList(newPostList)
    }
  }
  const scrapeMarkdown = async (post) => {
    return await fetch(`/api/wordpress_download_post`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(post)
    }).then((response) => response.json())
  }

  const addPost = async () => {
    for (let postCount in postList) {
      if (postCount > 0) {
        console.log('max reached')
        break
      }
      if (postList[postCount].markdown) {
        //if markdown is already scraped
        setProgress(postCount)
        // check if post already exist in strapi
        const response = await fetch(`/api/strapi_get_strapi_post`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(postList[postCount])
        }).then((response) => response.json())

        if (response.data.length === 0) {
          //if post does not exist in strapi, add post to strapi
          const response = await fetch(`/api/strapi_add_post`, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify(postList[postCount])
          }).then((response) => response.json())
          console.log('missing post')
        } else {
          console.log('post already exist')
        }

        // const post = await fetch(`/api/strapi_add_post`, { //add post to strapi
        //   method: 'POST',
        //   headers: {
        //     'Content-Type': 'application/json'
        //   },
        //   body: JSON.stringify(postList[postCount])
        // }).then((response) => response.json())
      }
    }
  }

  return (
    <div>
      <Head>
        <title>Wordpress Migrator</title>
      </Head>
      <main>
        <InputGroup size="sm">
          <InputLeftAddon children="https://" />
          <Input onChange={(e) => setUrl(e.target.value)} placeholder={url} />
          <InputRightAddon children=".com" />
        </InputGroup>
        {!loading && step === 'crawl' ? (
          <Button onClick={getPostJson} colorScheme="teal">
            Crawl Wordpress
          </Button>
        ) : (
          <Button onClick={getPostJson} isLoading colorScheme="teal">
            Crawl Wordpress
          </Button>
        )}
        {!loading && step === 'backup' ? (
          <Button onClick={convertMarkdown} colorScheme="teal">
            Convert Markdown
          </Button>
        ) : (
          <Button onClick={convertMarkdown} isLoading colorScheme="teal">
            Convert Markdown
          </Button>
        )}
        {!loading && step === 'strapi' ? (
          <Button onClick={addPost} colorScheme="teal">
            Import to Strapi
          </Button>
        ) : (
          <Button onClick={addPost} colorScheme="teal">
            Import Strapi
          </Button>
        )}
        <p>
          Total Pages: {postInfo.totalPages} - Total Post {postInfo.totalPost} -
          Post Progress: {progress}
        </p>
        ;
        <UnorderedList>
          {postList.map((item) => {
            return (
              <ListItem key={item.slug}>
                {item.markdown ? `markdown completed: ${item.slug}` : item.slug}
              </ListItem>
            )
          })}
        </UnorderedList>
      </main>
    </div>
  )
}

//{item.markdown && <CheckIcon />}
